#use wml::debian::template title="Las personas: quiénes somos y qué hacemos" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">¿Cómo empezó todo?<a>
    <li><a href="#devcont">Desarrolladores y contribuidores</a>
    <li><a href="#supporters">Individuos y organizaciones que apoyan a Debian</a>
    <li><a href="#users">Usuarios de Debian</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Ya que mucha gente lo ha preguntado, Debian se pronuncia <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/.</span> Se llama así por el creador de Debian, Ian Murdock, y por su esposa, Debra.</p>
</aside>

<h2><a id="history">¿Cómo empezó todo?</a></h2>

<p>Fue en agosto de 1993 cuando Ian Murdock empezó a trabajar en un nuevo
sistema operativo que se realizaría de manera abierta, en línea con el espíritu de Linux y
GNU. Envió una invitación abierta a otros desarrolladores de software pidiéndoles
que contribuyeran a una distribución de software basada en el núcleo Linux,
que era relativamente nuevo en aquel entonces. Debian estaba pensado para ser creado de
forma cuidadosa y concienzuda, y ser mantenido y soportado
con el mismo cuidado, abrazando un diseño abierto, contribuciones y soporte
procedentes de la comunidad de software libre.</p>

<p>Comenzó como un grupo de pocos y fuertemente unidos hackers de software libre y
creció gradualmente hasta convertirse en una comunidad grande y bien organizada de desarrolladores,
contribuidores y usuarios.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">Lea la Historia completa</a></button></p>

<h2><a id="devcont">Desarrolladores y contribuidores</a></h2>

<p>
Debian es una organización integrada en su totalidad por voluntarios y voluntarias. Más de mil desarrolladores
en activo repartidos <a href="$(DEVEL)/developers.loc">por todo el mundo</a>
trabajan en Debian en su tiempo libre. Pocos nos conocemos en
persona. Nos comunicamos, principalmente, a través de correo electrónico (listas de correo
en <a href="https://lists.debian.org/">lists.debian.org</a>) y de IRC
(canal #debian en irc.debian.org).
</p>

<p>
La lista completa de miembros oficiales de Debian se encuentra en
<a href="https://nm.debian.org/members">nm.debian.org</a>, y
<a href="https://contributors.debian.org">contributors.debian.org</a>
muestra una lista con todos los contribuidores y equipos que trabajan en la
distribución Debian.</p>

<p>El proyecto Debian tiene una <a href="organization">estructura organizada</a>
cuidadosamente. Si desea más información sobre cómo es el proyecto Debian por dentro,
visite el <a href="$(DEVEL)/">rincón del desarrollador</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Infórmese sobre nuestra filosofía</a></button></p>

<h2><a id="supporters">Individuos y organizaciones que apoyan a Debian</a></h2>

<p>Además de los desarrolladores y contribuidores, muchos otros individuos y
organizaciones forman parte de la comunidad Debian:

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Patrocinadores de alojamiento de servicios («hosting») y de hardware</a></li>
  <li><a href="../mirror/sponsors">Patrocinadores de sitios de réplica</a></li>
  <li><a href="../partners/">Socios de desarrollo y de servicios</a></li>
  <li><a href="../consultants">Consultores</a></li>
  <li><a href="../CD/vendors">Vendedores de medios de instalación de Debian</a></li>
  <li><a href="../distrib/pre-installed">Vendedores de ordenadores con Debian preinstalado</a></li>
  <li><a href="../events/merchandise">Vendedores de material promocional</a></li>
</ul>

<h2><a id="users">Usuarios de Debian</a></h2>

<p>
Una amplia variedad de organizaciones, grandes y pequeñas, así como muchos
miles de personas de forma individual usan Debian. En nuestra página <a href="../users/">¿Quién usa Debian?</a>
encontrará una lista de organizaciones educativas, empresariales y sin
ánimo de lucro, así como agencias gubernamentales, que han enviado una breve
reseña explicando cómo y por qué utilizan Debian.
</p>
