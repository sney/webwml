#use wml::debian::template title="Distribuzione degli sviluppatori" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="21849af3149d448e7ee39af170c93bee402c4d3a" maintainer="Giuseppe Sacco"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Dove si trovano gli
sviluppatori Debian (DD)? Se un DD ha indicato le coordinate di casa nel
database degli sviluppatori, viene visualizzato nella nostra mappa del
mondo.</p>
</aside>

<p>
La mappa sottostante è stata generata un <a
href="developers.coords">elenco delle coordinate degli sviluppatori</a>
anonimizzato usando il programma <a
href="https://packages.debian.org/stable/graphics/xplanet">xplanet</a>.
</p>

<img src="developers.map.jpeg" alt="Mappa del mondo">

<h2>Come aggiungere le proprie coordinate</h2>

<p>
Se si vogliono aggiungere le proprie coordinate ai propri dati nel database,
collegarsi al <a href="https://db.debian.org">database degli sviluppatori
Debian</a> e modificare il proprio record. Se non si conoscono le coordinate
della propria città, si può usare <a href="https://osm.org">OpenStreetMap</a>
per trovarle. Cercare la propria città e selezionare le frecce direzionali
accanto al campo di ricerca. Spostare il marcatore verde sulla mappa e le
coordinate appariranno nel campo <em>Da</em>.
</p>

<p>Il formato delle coordinate è uno dei seguenti:</p>

<dl>
<dt>Gradi decimali</dt>
  <dd>Il formato è <code>+-DDD.DDDDDDDDDDDDDDD</code>. Programmi quali
  Xearth e molti siti web di posizionamento lo usano. La precisione è
  limitata a 4 o 5 decimali.</dd>
<dt>Gradi minuti (DGM)</dt>
  <dd>Il formato è <code>+-DDDMM.MMMMMMMMMMMMM</code>. Non è un tipo aritmentico,
    ma una rappresentazione compatta di due unità distinte, gradi e minuti.
    Questo output è comune tra alcuni GPS portatili ed è utilizzato nel formato
    NMEA di messaggi GPS.</dd>
<dt>Gradi minuti secondi (DGMS)</dt>
<dd>Il formato è <code>+-DDDMMSS.SSSSSSSSSSS</code>. Come per DGM, non si tratta
    di un tipo aritmentico ma di una rappresentazione compatta di tre
    diverse unità: gradi, minuti e secondi. Questo output è utilizzato
    in alcuni siti web che danno tre diversi valori per ogni posizione.
    Ad esempio <code>34:50:12.24523 Nord</code> è rappresentato in DGMS come
    <code>+0345012.24523</code>.</dd>
</dl>

<p> 
<strong>Nota:</strong> per la latitudine <code>+</code> è il Nord,
per la longitudine <code>+</code> è l'Est. &Egrave;
importante specificare tutti gli zeri iniziali per evitare le
ambiguità sul formato nel caso che la tua posizione sia minore
di due gradi dal punto zero.
</p>
