#use wml::debian::template title="Введение в Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c" maintainer="Lev Lamberov"

<a id=community></a>
<h2>Debian &mdash; это сообщество людей</h2>
<p>Тысячи добровольцев со всего мира работают вместе, ставя приоритетом Свободное
  ПО и нужды пользователей.</p>

<ul>
  <li>
    <a href="people">Люди:</a>
    Кто мы, что мы делаем
  </li>
  <li>
    <a href="philosophy">Философия:</a>
    Почему мы это делаем и как мы это делаем
  </li>
  <li>
    <a href="../devel/join/">Принять участие, внести свою лепту:</a>
    Вы можете быть частью всего этого!
  </li>
  <li>
    <a href="help">Как вы можете помочь Debian?</a>
  </li>
  <li>
    <a href="../social_contract">Social Contract:</a>
    Наши моральные приоритеты
  </li>
  <li>
    <a href="diversity">Акт о многообразии</a>
  </li>
  <li>
    <a href="../code_of_conduct">Правила поведения</a>
  </li>
  <li>
    <a href="../partners/">Партнёры:</a>
    Компании и организации, предоставляющие продолжающуюся помощь Проекту
    Debian
  </li>
  <li>
    <a href="../donations">Пожертвования</a>
  </li>
  <li>
    <a href="../legal/">Юридическая информация</a>
  </li>
  <li>
    <a href="../legal/privacy">Защита персональных данных</a>
  </li>
  <li>
    <a href="../contact">Связаться с нами</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Debian является свободной операционной системой</h2>
<p>Мы начали с Linux и добавили много тысяч приложений, чтобы удовлетворить
  нужды наших пользователей.</p>

<ul>
  <li>
    <a href="../distrib">Скачать:</a>
    Больше вариантов образов Debian
  </li>
  <li>
  <a href="why_debian">Почему Debian</a>
  </li>
  <li>
    <a href="../support">Поддержка:</a>
    Получить помощь
  </li>
  <li>
    <a href="../security">Безопасность:</a>
    Последнее обновление<br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages"> Пакеты ПО:</a>
    Поиск и просмотр длинного списка нашего ПО
  </li>
  <li>
    <a href="../doc"> Документация</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Вики-страницы Debian</a>
  </li>
  <li>
    <a href="../Bugs"> Отчёты об ошибках</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Списки рассылки</a>
  </li>
  <li>
    <a href="../blends"> Чистые смеси:</a>
    Метапакеты для конкретных нужд
  </li>
  <li>
    <a href="../devel"> Уголок разработчика:</a>
    Информация, интересная в первую очередь для разработчиков Debian
  </li>
  <li>
    <a href="../ports"> Переносы/Архитектуры:</a>
    Поддерживаемые нами архитектуры ЦП
  </li>
  <li>
    <a href="search">Информация о том, как использовать поисковый движок Debian</a>.
  </li>
  <li>
    <a href="cn">Информация о страницах, доступных на нескольких языках</a>.
  </li>
</ul>
