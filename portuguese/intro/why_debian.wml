#use wml::debian::template title="Razões para escolher o Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian para usuários(as)</a></li>
    <li><a href="#devel">Debian para desenvolvedores(as)</a></li>
    <li><a href="#enterprise">Debian para ambientes corporativos</a></li>
  </ul>
</div>

<p>
Existem muitos motivos para escolher o Debian como seu sistema operacional –
como usuário(a), desenvolvedor(a) e até mesmo em ambientes corporativos. A
maioria dos(as) usuários(as) aprecia a estabilidade e os processos de
atualização suaves dos pacotes e de toda a distribuição. O Debian também é
amplamente usado por desenvolvedores(as) de software e hardware porque roda em
várias arquiteturas e dispositivos, oferece um rastreador público de bugs e
outras ferramentas para desenvolvedores(as). Se você planeja usar o Debian em
um ambiente profissional, há benefícios adicionais como versões LTS e imagens
cloud.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span>Para mim, é o nível perfeito
de facilidade de uso e estabilidade. Eu usei várias distribuições diferentes ao
longo dos anos, mas o Debian é o único que simplesmente funciona. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx?utm_source=share&utm_medium=web2x&context=3">NorhamsFinest no Reddit</a></p>
</aside>

<h2><a id="users">Debian para usuários(as)</a></h2>

<p>
<dl>
  <dt><strong>O Debian é software livre</strong></dt>
  <dd>
    O Debian é feito de software livre e de código aberto, e sempre será 100%
    <a href="free">livre</a>. Livre para qualquer pessoa usar, modificar e
    distribuir. Esta é nossa principal promessa para
    <a href="../users">nossos(as) usuários(as)</a>. Ele também é livre de custo.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian é estável e seguro</strong></dt>
  <dd>
    O Debian é um sistema operacional baseado no Linux para uma ampla variedade
    de dispositivos, incluindo laptops, desktops e servidores. Fornecemos uma
    configuração padrão razoável para cada pacote, bem como atualizações de
    segurança regulares durante a vida útil dos pacotes.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian tem um vasto suporte a hardware</strong></dt>
  <dd>
    A maior parte do hardware é suportado pelo kernel Linux, o que significa que
    o Debian também irá suportá-lo. Se necessário, drivers proprietários para
    hardware estão disponíveis.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian fornece um instalador flexível</strong></dt>
  <dd>
    Nosso
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live CD</a>
    é útil para todas as pessoas que desejam experimentar o Debian antes de
    instalá-lo. Ele também inclui o instalador Calamares que torna fácil
    instalar o Debian a partir do sistema live. Usuários(as) mais experientes
    podem usar o instalador Debian com mais opções de ajuste fino, incluindo a
    possibilidade de usar uma ferramenta de instalação de rede automatizada.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian fornece atualizações suaves</strong></dt>
  <dd>
    É fácil manter nosso sistema operacional atualizado, se você deseja
    atualizar para uma versão completamente nova ou apenas atualizar um único
    pacote.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian é a base para muitas outras distribuições</strong></dt>
  <dd>
    Muitas distribuições Linux populares, como Ubuntu, Knoppix, PureOS,
    SteamOS ou Tails, são baseadas no Debian. Fornececemos todas as ferramentas
    para que qualquer pessoa possa estender os pacotes de software a partir do
    repositório Debian, com seus próprios pacotes se necessário.
  </dd>
</dl>

<dl>
  <dt><strong>O projeto Debian é uma comunidade</strong></dt>
  <dd>
    Qualquer pessoa pode fazer parte da nossa comunidade; você não precisa ser
    um(a) desenvolvedor(a) ou um(a) administrador(a) de sistema. O Debian tem
    uma <a href="../devel/constitution">estrutura de governança democrática</a>.
    Como todos(as) os(as) membros(as) do projeto Debian possuem direitos iguais,
    o Debian não pode ser controlado por uma única empresa. Nossos(as)
    desenvolvedores(as) estão em mais de 60 países, e o Debian é traduzido para
    mais de 80 idiomas.
  </dd>
</dl>
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span>A razão por trás do status do
Debian como sistema operacional de desenvolvedor(a) é o grande número de pacotes
e software de suporte, que são importantes para desenvolvedores(as). É
altamente recomendado para programadores(as) avançados(as) e administradores(as)
de sistema.
<a href="https://fossbytes.com/best-linux-distros-for-programming-developers/"> Adarsh Verma em Fossbytes.</a></p>
</aside>

<h2><a id="devel">Debian para desenvolvedores(as)</a></h2>

<p>
<dl>
  <dt><strong>Várias arquiteturas de hardware</strong></dt>
  <dd>
    O Debian oferece suporte a uma <a href="../ports">longa lista</a> de
    arquiteturas de CPU, incluindo amd64, i386, várias versões de ARM e MIPS,
    POWER7, POWER8, IBM System z e RISC-V. O Debian também está disponível
    para arquiteturas de nicho.
  </dd>
</dl>

<dl>
  <dt><strong>IoT e dispositivos embarcados</strong></dt>
  <dd>
    O Debian roda em uma ampla variedade de dispositivos, como o Raspberry Pi,
    variantes do QNAP, dispositivos móveis, roteadores domésticos e muitos
    computadores de placa única (SBC - Single Board Computers).
  </dd>
</dl>

<dl>
  <dt><strong>Grande número de pacotes de software </strong></dt>
  <dd>
    O Debian tem um grande número de
    <a href="$(DISTRIB)/packages">pacotes</a> (atualmente na versão estável
    - stable: <packages_in_stable> pacotes) que usam o formato
    <a href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">deb</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Versões diferentes</strong></dt>
  <dd>
    Além da versão estável (stable) do Debian, você pode instalar versões
    mais novas dos softwares usando as versões teste (testing) ou instável
    (unstable).
  </dd>
</dl>

<dl>
  <dt><strong>Rastreador de bugs público</strong></dt>
  <dd>
    Nosso <a href="../Bugs">sistema de rastreamento de bugs (BTS - Bug Tracking System)</a>
    do Debian está disponível publicamente para todas as pessoas por meio de um
    navegador web. Não escondemos nossos bugs de software, e você pode
    facilmente enviar novos relatórios de bug ou participar das discussões.
  </dd>
</dl>

<dl>
  <dt><strong>Políticas do Debian e ferramentas para desenvolvedores(as)</strong></dt>
  <dd>
    O Debian oferece software de alta qualidade. Para aprender mais sobre
    nossos padrões, leia a <a href="../doc/debian-policy/">política</a> que
    define os requisitos técnicos para cada pacote incluído na distribuição.
    Nossa estratégia de integração contínua envolve o Autopkgtest (executa
    testes em pacotes), o Piuparts (testa instalação, atualização e remoção) e o
    Lintian (verifica os pacotes em busca de inconsistências e erros).
  </dd>
</dl>
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span>Estabilidade é sinônimo de Debian. [...]
Segurança é um dos recursos mais importantes do Debian.
<a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis on pontikis.net</a>
</p>
</aside>

<h2><a id="enterprise">Debian para ambientes corporativos</a></h2>

<p>
<dl>
  <dt><strong>O Debian é confiável</strong></dt>
  <dd>
    O Debian confirma sua confiabilidade a cada dia em milhares de cenários
    reais, desde um laptop de usuário(a) até aceleradores de partículas,
    passando pelo mercado financeiro e pela indústria automotiva. Também é
    popular no mundo acadêmico, na ciência e no setor público.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian tem muitos(as) especialistas</strong></dt>
  <dd>
    Nossos(as) mantenedores(as) de pacotes não apenas tomam conta do
    empacotamento do Debian e incorporação de novas versões de aplicativos.
    Frequentemente eles(as) são especialistas nos softwares originais e
    contribuem diretamente para o desenvolvimento desses softwares.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian é seguro</strong></dt>
  <dd>
    O Debian tem suporte à segurança para suas versões estáveis (stable). Muitas
    outras distribuições e pesquisadores(as) de segurança confiam no rastreador
    de segurança do Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Suporte de longo prazo</strong></dt>
  <dd>
    A versão gratuita do <a href="https://wiki.debian.org/LTS">suporte de longo prazo (LTS - Long Term Support)</a>
    estende a vida útil de todas as versões estáveis (stable) por pelo menos 5
    anos. Além disso, a iniciatiava comercial
    <a href="https://wiki.debian.org/LTS/Extended">LTS Estendido (Extended LTS)</a>
    oferece suporte a um conjunto limitado de pacotes para além de 5 anos.
  </dd>
</dl>

<dl>
  <dt><strong>Imagens para computação em nuvem</strong></dt>
  <dd>
    Imagens oficiais para computação em nuvem estão disponíveis para todas as
    principais plataformas de nuvem. Também fornecemos as ferramentas e
    configurações para que você possa construir sua própria imagem personalizada
    para computação em nuvem. Você também pode usar o Debian em máquinas
    virtuais em um desktop ou em um contêiner.
  </dd>
</dl>
</p>

