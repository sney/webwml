#use wml::debian::template title="Debian 6.0 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="733099de17cfc0457fd3bc1709a74fd22f1a8e53"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemas conhecidos</toc-add-entry>
<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>O Debian 6.0 'squeeze' alcançou o final de sua linha do tempo regular de
suporte de segurança. No entanto, a
<a href="https://wiki.debian.org/LTS">equipe Squeeze LTS</a> fornece suporte
contínuo de segurança para esta versão.</p>

<p>Observe que essas atualizações não são distribuídas pelos espelhos de
segurança normais; em vez disso, você precisa adicionar o repositório
<q>squeeze-lts</q> para usá-los.</p>

<p>Se você usa o APT, adicione a seguinte linha ao <tt>/etc/apt/sources.list</tt>
para poder acessar as atualizações de segurança mais recentes:</p>

<pre>
  deb http://http.debian.net/debian/ squeeze-lts main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Lançamentos pontuais</toc-add-entry>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão já lançada é atualizada. Geralmente eles são indicados como
lançamentos pontuais.</p>

<ul>
  <li>A primeira versão pontual, 6.0.1, foi lançada em
      <a href="$(HOME)/News/2011/20110319">19 de março de 2011</a>.</li>
  <li>A segunda versão pontual, 6.0.2, foi lançada em
      <a href="$(HOME)/News/2011/20110625">25 de junho de 2011</a>.</li>
  <li>A terceira versão pontual, 6.0.3, foi lançada em
      <a href="$(HOME)/News/2011/20111008">8 de outubro de 2011</a>.</li>
  <li>A quarta versão pontual,, 6.0.4, foi lançada em
      <a href="$(HOME)/News/2012/20120128">28 de janeiro de 2012</a>.</li>
  <li>A quinta versão pontual, 6.0.5, foi lançada em
      <a href="$(HOME)/News/2012/20120512">12 de maio de 2012</a>.</li>
  <li>A sexta versão pontual, 6.0.6, foi lançada em
      <a href="$(HOME)/News/2012/20120929">29 de setembro de 2012</a>.</li>
  <li>A sétima versão pontual, 6.0.7, foi lançada em
      <a href="$(HOME)/News/2013/20130223">23 de fevereiro de 2013</a>.</li>
  <li>A oitava versão pontual, 6.0.8, foi lançada em
      <a href="$(HOME)/News/2013/20131020">20 de outubro de 2013</a>.</li>
  <li>A nona versão pontual, 6.0.9, foi lançada em
      <a href="$(HOME)/News/2014/20140215">15 de fevereiro de 2014</a>.</li>
  <li>A décima versão pontua, 6.0.10, foi lançada em
      <a href="$(HOME)/News/2014/20140719">19 de julho de 2014</a>.</li>
</ul>

<ifeq <current_release_squeeze> 6.0.0 "

<p>Ainda não há lançamentos pontuais para o Debian 6.0.</p>" "

<p>Consulte o <a
href=http://http.us.debian.org/debian/dists/squeeze/ChangeLog>\
ChangeLog</a>
para obter detalhes sobre alterações entre 6.0.0 e <current_release_squeeze/>.</p>"/>

<p>As correções na versão estável (stable) lançada geralmente passam por um
longo período de teste antes de serem aceitas no repositório.
No entanto, essas correções estão disponíveis no repositório
<a href="http://ftp.debian.org/debian/dists/squeeze-proposed-updates/">\
dists/squeeze-proposed-updates</a> de qualquer espelho do repositório Debian.</p>

<p>Se você usar o APT para atualizar seus pacotes, poderá instalar as
atualizações propostas adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# Atualizações propostas para a versão 6.0
  deb http://ftp.us.debian.org/debian squeeze-proposed-updates main contrib non-free
</pre>

<p>Depois disso, execute  <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>

<toc-add-entry name="installer">Sistema de instalação</toc-add-entry>

<p>
Para obter informações sobre erratas e atualizações para o sistema de instalação,
consulte a página <a href="debian-installer/">informações de instalação</a> .
</p>

