# Translation of ports.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2013.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2013-05-14 13:36+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian pre Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian pre PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "CD Hurd"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian pre IA-64"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "Kontakt"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "Procesory"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Zásluhy"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Vývoj"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Dokumentácia"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Inštalácia"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr "Konfigurácia"

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Odkazy"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Novinky"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Portovanie"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Porty"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Problémy"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Mapa softvéru"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Stav"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Dodávka"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Systémy"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "Debian GNU/NetBSD pre i386"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "Debian GNU/NetBSD pre Alpha"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "Prečo"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "Ľudia"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian pre PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian pre Sparc"

#~ msgid "Debian for Laptops"
#~ msgstr "Debian pre notebooky"

#~ msgid "Debian for AMD64"
#~ msgstr "Debian pre AMD64"

#~ msgid "Debian for ARM"
#~ msgstr "Debian pre ARM"

#~ msgid "Debian for Beowulf"
#~ msgstr "Debian pre Beowulf"

#~ msgid "Main"
#~ msgstr "Hlavné"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "Debian GNU/FreeBSD"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "Debian pre Motorola 680x0"

#~ msgid "Debian for MIPS"
#~ msgstr "Debian pre MIPS"

#~ msgid "Debian for S/390"
#~ msgstr "Debian pre S/390"

#~ msgid "Debian for Sparc64"
#~ msgstr "Debian pre Sparc64"
