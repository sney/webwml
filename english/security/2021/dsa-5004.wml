<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in XStream, a Java
library to serialize objects to XML and back again.</p>

<p>These vulnerabilities may allow a remote attacker to load and execute arbitrary
code from a remote host only by manipulating the processed input stream.</p>

<p>XStream itself sets up a whitelist by default now, i.e. it blocks all classes
except those types it has explicit converters for. It used to have a blacklist
by default, i.e. it tried to block all currently known critical classes of the
Java runtime. Main reason for the blacklist were compatibility, it allowed to
use newer versions of XStream as drop-in replacement. However, this approach
has failed. A growing list of security reports has proven, that a blacklist is
inherently unsafe, apart from the fact that types of 3rd libraries were not
even considered. A blacklist scenario should be avoided in general, because it
provides a false sense of security.</p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 1.4.11.1-1+deb10u3.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1.4.15-3+deb11u1.</p>

<p>We recommend that you upgrade your libxstream-java packages.</p>

<p>For the detailed security status of libxstream-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5004.data"
# $Id: $
