<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy discovered a vulnerability in the Transmission BitTorrent
client; insecure RPC handling between the Transmission daemon and the
client interface(s) may result in the execution of arbitrary code if a
user visits a malicious website while Transmission is running.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.52-3+nmu3.</p>

<p>We recommend that you upgrade your transmission packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1246.data"
# $Id: $
