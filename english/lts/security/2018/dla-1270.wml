<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor, which
could result in privilege escalation.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.lts1-12.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>Please note that <a href="https://security-tracker.debian.org/tracker/CVE-2017-15590">CVE-2017-15590</a> (XSA-237) will *not* be fixed in wheezy as
the patches are too intrusive to backport.
The vulnerability can be mitigated by not passing through physical devices
to untrusted guests.
More information can be found on <a href="https://xenbits.xen.org/xsa/advisory-237.html">https://xenbits.xen.org/xsa/advisory-237.html</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1270.data"
# $Id: $
