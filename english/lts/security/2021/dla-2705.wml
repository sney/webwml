<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues have been discovered in scilab, particularly in ezXML embedded library:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30485">CVE-2021-30485</a>

    <p>Descriptionincorrect memory handling, leading to a NULL pointer dereference
    in ezxml_internal_dtd()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31229">CVE-2021-31229</a>

    <p>Out-of-bounds write in ezxml_internal_dtd() leading to out-of-bounds write
    of a one byte constant</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31347">CVE-2021-31347</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-31348">CVE-2021-31348</a>

    <p>incorrect memory handling in ezxml_parse_str() leading to out-of-bounds read</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31598">CVE-2021-31598</a>

    <p>Out-of-bounds write in ezxml_decode() leading to heap corruption</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
5.5.2-4+deb9u1.</p>

<p>We recommend that you upgrade your scilab packages.</p>

<p>For the detailed security status of scilab please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/scilab">https://security-tracker.debian.org/tracker/scilab</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2705.data"
# $Id: $
