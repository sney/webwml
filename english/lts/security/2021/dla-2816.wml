<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Icinga 2, a general-purpose
monitoring application. An attacker could retrieve sensitive
information such as service passwords and ticket salt by querying the
web API, or by intercepting unsufficiently checked encrypted
connections.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32739">CVE-2021-32739</a>

    <p>A vulnerability exists that may allow privilege escalation for
    authenticated API users. With a read-ony user's credentials, an
    attacker can view most attributes of all config objects including
    `ticket_salt` of `ApiListener`. This salt is enough to compute a
    ticket for every possible common name (CN). A ticket, the master
    node's certificate, and a self-signed certificate are enough to
    successfully request the desired certificate from Icinga. That
    certificate may in turn be used to steal an endpoint or API user's
    identity.  See also complementary manual procedures:
    <a href="https://icinga.com/blog/2021/07/15/releasing-icinga-2-12-5-and-2-11-10/#change-ticket-salt">https://icinga.com/blog/2021/07/15/releasing-icinga-2-12-5-and-2-11-10/#change-ticket-salt</a>
    <a href="https://icinga.com/blog/2021/07/15/releasing-icinga-2-12-5-and-2-11-10/#replace-icinga-ca">https://icinga.com/blog/2021/07/15/releasing-icinga-2-12-5-and-2-11-10/#replace-icinga-ca</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32743">CVE-2021-32743</a>

    <p>Some of the Icinga 2 features that require credentials for
    external services expose those credentials through the API to
    authenticated API users with read permissions for the
    corresponding object types. IdoMysqlConnection and
    IdoPgsqlConnection exposes the password of the user used to
    connect to the database. An attacker who obtains these credentials
    can impersonate Icinga to these services and add, modify and
    delete information there. If credentials with more permissions are
    in use, this increases the impact accordingly.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37698">CVE-2021-37698</a>

    <p>InfluxdbWriter and Influxdb2Writer do not verify the server's
    certificate despite a certificate authority being
    specified. Icinga 2 instances which connect to any of the
    mentioned time series databases (TSDBs) using TLS over a spoofable
    infrastructure should immediately upgrade. Such instances should
    also change the credentials (if any) used by the TSDB writer
    feature to authenticate against the TSDB.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.6.0-2+deb9u2.</p>

<p>We recommend that you upgrade your icinga2 packages.</p>

<p>For the detailed security status of icinga2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/icinga2">https://security-tracker.debian.org/tracker/icinga2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2816.data"
# $Id: $
