<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities have been fixed in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12418">CVE-2019-12418</a>

	<p>When Apache Tomcat is configured with the JMX Remote Lifecycle
        Listener, a local attacker without access to the Tomcat process
        or configuration files is able to manipulate the RMI registry to
        perform a man-in-the-middle attack to capture user names and
        passwords used to access the JMX interface. The attacker can
        then use these credentials to access the JMX interface and gain
        complete control over the Tomcat instance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17563">CVE-2019-17563</a>

	<p>When using FORM authentication with Apache Tomcat there was a
        narrow window where an attacker could perform a session fixation
        attack. The window was considered too narrow for an exploit to
        be practical but, erring on the side of caution, this issue has
        been treated as a security vulnerability.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7.0.56-3+really7.0.99-1.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2077.data"
# $Id: $
