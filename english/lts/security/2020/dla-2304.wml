<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>`add_password` in pam_radius_auth.c in pam_radius 1.4.0 does not
correctly check the length of the input password, and is vulnerable
to a stack-based buffer overflow during memcpy(). An attacker could
send a crafted password to an application (loading the pam_radius
library) and crash it. Arbitrary code execution might be possible,
depending on the application, C library, compiler, and other factors.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.3.16-5+deb9u1.</p>

<p>We recommend that you upgrade your libpam-radius-auth packages.</p>

<p>For the detailed security status of libpam-radius-auth please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libpam-radius-auth">https://security-tracker.debian.org/tracker/libpam-radius-auth</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2304.data"
# $Id: $
