<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in mupdf, a lightweight PDF viewer.</p>

<p>The issues could be exploited by crafted PDF files that result in denial
of service by heap-based buffer overflows, segmentation faults or out of
bound reads.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.9a+ds1-4+deb9u5.</p>

<p>We recommend that you upgrade your mupdf packages.</p>

<p>For the detailed security status of mupdf please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mupdf">https://security-tracker.debian.org/tracker/mupdf</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2289.data"
# $Id: $
