<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in curl, a command line tool for transferring data
with URL syntax.
In rare circumstances, when using the multi API of curl in combination
with CURLOPT_CONNECT_ONLY, the wrong connection  might be used when
transfering data later.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
7.52.1-5+deb9u12.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2382.data"
# $Id: $
