msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2021-01-20 14:52+0100\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: \n"
"X-Generator: Poedit 2.3.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "e-post med delegering"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "e-post med utpeking"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegat"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>han/ham"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>hun/henne"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>delegate"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>de/dem"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "nåværende"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "medlem"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "bestyrer"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Koordinator for stabil utgave"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "wizard"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "styreformann"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "sekretær"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "representativ"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "rolle"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"I den følgende listen er <q>nåværende</q> brukt for posisjoner\n"
"som er valgt eller utpekt med en bestemt utløpsdato."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Styremedlemmer"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribusjon"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr "Kommunikasjon og oppsøkende virksomhet"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr "Databeskyttelseslag"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "Publisitetslag"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr "Medlemskap i andre organisasjoner"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Støtte og infrastruktur"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Leder"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Teknisk komite"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Sekretær"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Utviklingsprosjekter"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP-arkiv"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "FTP-ansvarlige"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "FTP-assistenter"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "FTP-trollmenn"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Backports-lag"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Utgavekoordinering"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Utgiverlag"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Kvalitetskontroll"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Installasjonsystems-gruppen"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr "Debian Live-lag"

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Utgivelsesnotater"

#: ../../english/intro/organization.data:152
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "CD/DVD/USB-bilder"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Produksjon"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Testing"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr "Cloud-lag"

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "Infrastruktur for automatisk bygging"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Buildd-administrasjon"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Dokumentasjon"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "List over pakker som trenger arbeid og mulige pakker"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Pressekontakt"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Nettsider"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr "Oppsøkende"

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Debian kvinneprosjekt"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr "Fellesskap"

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Begivenheter"

#: ../../english/intro/organization.data:264
msgid "DebConf Committee"
msgstr "DebConf komite"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Partnerprogram"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Koordinering av maskinvaredonasjon"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Feilrapportsystem"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Postlisteadministrasjon og -arkiv"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "Resepsjon for nye medlemmer"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Debian kontoforvaltere"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Nøkkelring-koordinatore (PGP og GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Sikkerhetsgruppen"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Retningslinjer"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Systemadministrasjon"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Dette er adressen du kan bruke når du kommer imot problemer med Debians "
"maskiner, slik som passordproblemer eller behov for å ha en særskilt pakke "
"installert."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Om du har problemer med Debians maskinvare, se på <a href=\"https://db."
"debian.org/machines.cgi\">Debians maskinside</a>. Denne skulle inneholde "
"adminstrator-opplysninger for hver maskin."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "Administrator av LDAP-utviklerlisten"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Blåkopier"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "DNS-forvalter"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Pakkesporingssystem"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr "Kasserer"

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:392
msgid "Salsa administrators"
msgstr "Salsa-administratorer"

#~ msgid "Individual Packages"
#~ msgstr "Individuelle pakker"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian for barn mellom 1 og 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian for medisinsk praksis og forskning"

#~ msgid "Debian for education"
#~ msgstr "Debian for utdanning"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian på juridiske kontor"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian for folk med funksjonshemning"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian for naturvitenskap og relatert forskning"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian for utdanning"

#~ msgid "Live System Team"
#~ msgstr "Gruppen for «live»-system"

#~ msgid "Auditor"
#~ msgstr "Revisor"

#~ msgid "Publicity"
#~ msgstr "Publisitet"

#, fuzzy
#~| msgid "Debian Maintainer Keyring Team"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Debian utviklernøkkelringlag"

#, fuzzy
#~| msgid "Release Team"
#~ msgid "Volatile Team"
#~ msgstr "Utgiverlag"

#~ msgid "Vendors"
#~ msgstr "Leverandører"

#, fuzzy
#~ msgid "Release Assistants"
#~ msgstr "Utgavekoordinering"

#~ msgid "Mailing List Archives"
#~ msgstr "Postliste-arkiver"

#~ msgid "Mailing list"
#~ msgstr "Postliste"

#~ msgid "Installation"
#~ msgstr "Installasjon"

#~ msgid "Delegates"
#~ msgstr "Delegater"

#, fuzzy
#~ msgid "Installation System for ``stable''"
#~ msgstr "Installasjonsystems-gruppen"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian for ikke-kommersielle organisasjoner"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Det universelle operativsystemet som ditt skrivebord"

#~ msgid "Accountant"
#~ msgstr "Regnskapsfører"

#~ msgid "Key Signing Coordination"
#~ msgstr "Koordinering av nøkkelsignering"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Navnene på de individuelle buildd-administratorene kan fins også på <a "
#~ "href=\"http://www.buildd.net\">http://www.buildd.net</a>. Velg en "
#~ "arkitektur og en distribusjon for å se tilgjengelig buildd-er og deres "
#~ "administrator."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Administratorene har ansvaret for at buildd for en spesiell arkitektur "
#~ "(arch) kan nåes på <genericemail arch@buildd.debian.org>, for eksempel "
#~ "<genericemail i386@buildd.debian.org>."

#~ msgid "Marketing Team"
#~ msgstr "Markedsføringslag"

#~ msgid "Handhelds"
#~ msgstr "Håndholdte"

#~ msgid "APT Team"
#~ msgstr "APT-gruppen"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Utgiverlag for den «stabile» utgaven"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Tilpassede Debian distribusjoner"

#~ msgid "current Debian Project Leader"
#~ msgstr "nåværende leder for Debian-prosjektet"

#~ msgid "Security Audit Project"
#~ msgstr "Sikkerhetsvurderingsprosjekt"

#~ msgid "Testing Security Team"
#~ msgstr "Lag for sikkerhet testing"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth-administratorer"

#~ msgid "User support"
#~ msgstr "Brukerstøtte"

#~ msgid "Embedded systems"
#~ msgstr "Innebygde system"

#~ msgid "Firewalls"
#~ msgstr "Brannvegger"

#~ msgid "Laptops"
#~ msgstr "Bærbare"

#~ msgid "Special Configurations"
#~ msgstr "Spesielle systemer"

#~ msgid "Ports"
#~ msgstr "Porteringer"

#~ msgid "CD Vendors Page"
#~ msgstr "Side med CD-leverandører"

#~ msgid "Consultants Page"
#~ msgstr "Konsulentsiden"
