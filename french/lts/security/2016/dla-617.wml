#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans
libarchive, une bibliothèque d'archive et de compression multi-format.
Un attaquant pourrait tirer avantage de ces défauts pour provoquer une
lecture hors limites ou un déni de service à l'encontre d'une application
utilisant la bibliothèque libarchive12 en se servant d'un fichier d'entrée
soigneusement contrefait.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8915">CVE-2015-8915</a>

<p>Paris Zoumpouloglou de Project Zero labs a découvert un défaut dans
bsdtar de libarchive. Avec un fichier contrefait, bsdtar peut réaliser une
lecture de mémoire hors limites qui mènera à une erreur de segmentation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7166">CVE-2016-7166</a>

<p>Alexander Cherepanov a découvert un défaut dans le traitement de la
compression de libarchive. Avec un fichier gzip contrefait, il est possible
d'obtenir que libarchive invoque une chaîne infinie de compresseurs gzip
jusqu'à ce que toute la mémoire soit épuisée ou qu'une autre limite de
ressource se fasse sentir.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.0.4-3+wheezy3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libarchive.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-617.data"
# $Id: $
