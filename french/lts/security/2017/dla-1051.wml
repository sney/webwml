#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le système de base de
données PostgreSQL :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7486">CVE-2017-7486</a>

<p>Andrew Wheelwright a découvert que les mappages d’utilisateur étaient
insuffisamment restreints.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7546">CVE-2017-7546</a>

<p>Dans quelques méthodes d’authentification des mots de passe vides étaient
acceptés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7547">CVE-2017-7547</a>

<p>Des mappages d’utilisateur pourraient divulguer des données à des
utilisateurs non privilégiés.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 9.1.24lts2-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-9.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1051.data"
# $Id: $
