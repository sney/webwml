#use wml::debian::translation-check translation="20c4c7542d0df64b1bb0b78c04f3ed88d52fbc75" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Buster RC3</define-tag>
<define-tag release_date>2019-07-03</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la troisième version candidate pour Debian 10 <q>Buster</q>.
</p>

<p>
Cette version candidate de l'installateur est faite pour valider quelque
changements de dernière minute, et s'assurer que les paquets recommandés
du noyau Linux sont installés correctement. C'est aussi un moyen pour 
revérifier que la configuration pour la création des images 
d'installation est prête pour la préparation des images officielles de
Buster dans quelques jours.
</p>

<p>
Les rapports d'installation pour cette version particulière RC 3 de
l'installateur Debian sont les bienvenus comme toujours, mais les
utilisateurs pourraient préférer partager le plaisir de
<a href="https://lists.debian.org/debian-cd/2019/06/msg00024.html">tester les
images d'installation officielles samedi !</a>
</p>



<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>base-installer :
    <ul>
      <li>activation de l'installation des paquets recommandés lors de
        l'installation du noyau (<a href="https://bugs.debian.org/929667">nº 929667</a>).</li>
    </ul>
  </li>
  <li>debian-installer-utils :
    <ul>
      <li>option APT toujours configurée si les options --{with,no}-recommends
        sont utilisées (<a href="https://bugs.debian.org/931287">nº 931287</a>).</li>
    </ul>
  </li>
  <li>debian-cd :
    <ul>
      <li>inclusion des notes de publication de Buster dans les images
        des DVD, USB 16 Go et BD.</li>
    </ul>
  </li>
  <li>grub2 :
    <ul>
      <li>ajout de shim-signed aux paquets recommandés pour les paquets
        grub-efi-{arm64,i386}-signed (<a href="https://bugs.debian.org/931038">nº 931038</a>).</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>76 langues sont prises en charge dans cette version ;</li>
  <li>La traduction est complète pour 39 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
